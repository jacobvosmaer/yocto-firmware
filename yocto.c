#include "midi.h"
#include <avr/interrupt.h>
#include <avr/io.h>

#define nelem(x) (sizeof(x) / sizeof(x[0]))

struct {
	uint8_t key, pulse;
} voicetab[] = {{255}, {36}, {38}, {41}, {43}, {47}, {37},
		{39},  {56}, {49}, {46}, {42}, {60}, {62}};

/* This ordering is special. It matches the front panel buttons, the trigger
 shift registers and the LED shift registers. It also indexes voicetab. */
enum voice_names {
	TRIG_AC, /* accent */
	TRIG_BD, /* bass drum */
	TRIG_SD, /* snare drum */
	TRIG_LT, /* low tom */
	TRIG_MT, /* mid tom */
	TRIG_HT, /* high tom */
	TRIG_RM, /* rimshot */
	TRIG_HC, /* hand clap */
	TRIG_CW, /* cowbell */
	TRIG_CY, /* cymbal */
	TRIG_OH, /* open hihat */
	TRIG_CH, /* closed hihat */
	TRIG_T1, /* trigger output T1 (LED only) */
	TRIG_T2	 /* trigger output T2 (LED only) */
};

#define PIN(x, y) {&PORT##x, &DDR##x, y}

struct {
	volatile uint8_t *const port;
	volatile uint8_t *const ddr;
	const uint8_t bit;
} pintab[] = {PIN(B, 2), PIN(B, 4), PIN(B, 3), PIN(D, 6),
	      PIN(B, 1), PIN(C, 6), PIN(A, 1), PIN(A, 0)};

enum pin_names {
	TRIG_CPU,     /* pin used to create 808 "common trigger" pulses */
	SR_OUT_LATCH, /* latch for 4x 74HC595 trigger and LED driver shift
			 registers */
	BUTTON_LATCH, /* latch for 3x 74HC165 button input shift registers */
	TRIGGER1,     /* pin to toggle PNP driver of "Trigger 1" output */
	TRIGGER2,     /* pin to toggle PNP driver of "Trigger 2" output */
	LED_ROLL,     /* pin that drives the "Roll" LED */
	ROTARY_INSTRUMENT, /* common of "Instrument" rotary switch */
	ROTARY_MODE	   /* common of "Mode" rotary switch */
};

enum { MASK_ROTARY = 0xf << 2, MASK_SCALE = 0xf << 2 };

void pin_set(uint8_t p, uint8_t high) {
	uint8_t mask = high ? 0xff : 0;
	*pintab[p].port |= mask & (1 << pintab[p].bit);
	*pintab[p].port &= mask | ~(1 << pintab[p].bit);
}

uint8_t spi_io(uint8_t data) {
	SPDR = data;
	while (!(SPSR & 1 << SPIF))
		;
	return SPDR;
}

uint8_t subtract(uint8_t *x, uint8_t y) {
	uint8_t old = *x;
	*x = *x < y ? 0 : *x - y;
	return old;
}

enum { DIAL_MODE, DIAL_INSTRUMENT };

uint8_t dial_read(uint8_t dial) {
	pin_set(ROTARY_INSTRUMENT, dial == DIAL_INSTRUMENT);
	pin_set(ROTARY_MODE, dial == DIAL_MODE);
	return (PINA & MASK_ROTARY) >> 2;
}

void set_scale_leds(uint8_t nibble) {
	PORTC |= (nibble << 2) & MASK_SCALE;
	PORTC &= (nibble << 2) | ~MASK_SCALE;
}

uint8_t uart_read(uint8_t *data) {
	uint8_t status = UCSR1A;
	if (!(status & 1 << RXC1))
		return 0;

	*data = UDR1;
	return !(status & (1 << FE1 | 1 << DOR1 | 1 << UPE1));
}

/* Dmux uses four signals: frame, clock, accent and trig. We ignore accent
 * because we can't use it in the Yocto v1 hardware. The frame and clock signals
 * come in on PORTD4 and PORTD5 respectively. The trig signal comes in on PORTB0
 * thanks to a jumper on the jacks board. */
enum { DMUX_FRAME = 1 << 4, DMUX_CLOCK = 1 << 5, DMUX_TRIG = 1 << 0 };

/* The dmux struct contains private state used by the PCINT3 pin change
 * interrupt handler. */
struct {
	uint16_t bitmap;
	uint8_t state, n;
} dmux;

/* We use dmux_queue to send complete dmux messages ("frames") from the
 * interrupt handler to the main thread. */
struct {
	uint16_t frame[2];
	uint8_t idx;
} dmux_queue;

ISR(PCINT3_vect) {
	uint8_t new_state = PIND;

	if (!(dmux.state & DMUX_FRAME) &&
	    (new_state & DMUX_FRAME)) { /* frame rise */
		dmux.n = 0;
		dmux.bitmap = 0;
	} else if ((dmux.state & DMUX_FRAME) &&
		   !(new_state & DMUX_FRAME)) { /* frame fall */
		if (dmux.bitmap) {
			dmux_queue.idx =
			    (dmux_queue.idx + 1) % nelem(dmux_queue.frame);
			dmux_queue.frame[dmux_queue.idx] = dmux.bitmap;
		}
	} else if (!(dmux.state & DMUX_CLOCK) &&
		   (new_state & DMUX_CLOCK)) { /* clock rise */
		;
	} else if ((dmux.state & DMUX_CLOCK) &&
		   !(new_state & DMUX_CLOCK)) { /* clock fall */
		if ((PINB & DMUX_TRIG) && dmux.n < nelem(voicetab))
			dmux.bitmap |= (1 << dmux.n);
		dmux.n++;
	}

	dmux.state = new_state;
}

int main(void) {
	enum { PULSE = 18 }; /* pulse width: 1.2ms */
	midi_parser parser = {0};
	uint8_t i, time, led_roll, last_dmux_idx = 0;
	enum { BUTTON_SHIFT_REGISTERS = 3 };
	uint8_t debouncer[BUTTON_SHIFT_REGISTERS * 8];

	for (i = 0; i < nelem(pintab); i++)
		*pintab[i].ddr |= 1 << pintab[i].bit;

	DDRA &= ~MASK_ROTARY;  /* rotary bits are input */
	PORTA &= ~MASK_ROTARY; /* disable pull-ups */

	DDRC |= MASK_SCALE; /* scale bits are output */

	/* Run Timer0 at F_CPU/1024 Hz */
	TCCR0B = 1 << CS02 | 1 << CS00;

	/* Receive MIDI at UART1 */
	UBRR1H = 0;
	UBRR1L = 31;
	UCSR1B = 1 << RXEN1;

	/* Enable SPI */
	DDRB |= 1 << PORTB7 | 1 << PORTB5; /* SCK and SDO as output */
	SPCR |= 1 << SPE | 1 << MSTR;
	SPSR |= 1 << SPI2X;

	/* Configure the three DMUX GPIO's as inputs. */
	DDRD &= ~(DMUX_FRAME | DMUX_CLOCK);
	PORTD &= ~(DMUX_FRAME | DMUX_CLOCK);
	DDRB &= ~DMUX_TRIG;
	PORTB &= ~DMUX_TRIG;

	/* We use a pin change ("PC") interrupt to listen for signals on the
	 * dmux clock and frame lines. */
	PCMSK3 = DMUX_CLOCK | DMUX_FRAME;
	PCICR = (1 << PCIE3);
	sei();

	time = TCNT0;
	for (;;) {
		midi_message msg;
		uint8_t uart_data, delta, midi_channel, accent = 0, note_on = 0,
							dmux_idx;

		midi_channel = dial_read(DIAL_MODE);
		set_scale_leds(midi_channel);
		pin_set(TRIG_CPU, 0);

		if (dmux_idx = dmux_queue.idx, dmux_idx != last_dmux_idx) {
			for (i = 0; i < nelem(voicetab); i++)
				if (dmux_queue.frame[dmux_idx] & (1 << i))
					voicetab[i].pulse = PULSE;
			note_on = 1;
			last_dmux_idx = dmux_idx;
		}

		if (uart_read(&uart_data) &&
		    (msg = midi_read(&parser, uart_data),
		     msg.status == (MIDI_NOTE_ON | midi_channel)) &&
		    msg.data[1]) {
			uint8_t key = msg.data[0], velocity = msg.data[1];

			/* Blink "Roll" LED to show incoming MIDI note */
			led_roll = PULSE;

			for (i = 0; i < nelem(voicetab); i++) {
				if (key == voicetab[i].key) {
					voicetab[i].pulse = PULSE;
					note_on = 1;
					accent |= velocity >= 100;
				}
			}
		}

		delta = TCNT0 - time;
		time += delta;

		if (delta) { /* Limit debouncer update rate */
			uint8_t sregister;

			pin_set(BUTTON_LATCH, 1);
			for (i = 0; i < nelem(debouncer); i++) {
				if (!(i % 8))
					sregister = spi_io(0);
				debouncer[i] <<= 1;
				debouncer[i] |= sregister & 1;
				sregister >>= 1;

				if (i < nelem(voicetab) &&
				    debouncer[i] == 0x7f) {
					voicetab[i].pulse = PULSE;
					note_on = 1;
				}
			}
			pin_set(BUTTON_LATCH, 0);

			accent |= debouncer[TRIG_AC] == 0xff;
		}

		if (accent)
			voicetab[TRIG_AC].pulse = PULSE;

		if (delta || note_on) {
			uint16_t bitmap = 0;

			/* "Roll" LED is active high (driven directly by MCU).
			 */
			pin_set(LED_ROLL, subtract(&led_roll, delta));

			for (i = 0; i < nelem(voicetab); i++)
				bitmap |= !!subtract(&voicetab[i].pulse, delta)
					  << i;

			pin_set(SR_OUT_LATCH, 0);
			/* 74HC595 trigger outputs */
			spi_io(bitmap >> 8);
			spi_io(bitmap);
			/* 74HC595 LED outputs */
			spi_io(bitmap >> 8);
			spi_io(bitmap);
			pin_set(SR_OUT_LATCH, 1);
			pin_set(TRIG_CPU, note_on);
			note_on = 0;

			/* Trigger 1 & 2 are active low (driven by PNP
			 * transistors). */
			pin_set(TRIGGER1, !(bitmap & (1 << TRIG_T1)));
			pin_set(TRIGGER2, !(bitmap & (1 << TRIG_T2)));
		}
	}

	return 0;
}

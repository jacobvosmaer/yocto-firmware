#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define Write(buf)                                                             \
	if (fwrite(buf, 1, sizeof(buf), stdout) < sizeof(buf))                 \
		exit(1);

enum { PAGE_SIZE = 256 };
uint8_t buf[PAGE_SIZE + 1];

int main(void) {
	uint8_t sysex_start[] = {0xf0, 0x7d, 0x08, 0x08, 0x02},
		sysex_end[] = {0xf7}, write_page[] = {0x7e, 0x00},
		finalize[] = {0x7f, 0x00};
	int i, m;

	while ((m = fread(buf, 1, PAGE_SIZE, stdin))) {
		while (m < PAGE_SIZE)
			buf[m++] = 0;

		buf[PAGE_SIZE] = 0;
		for (i = 0; i < PAGE_SIZE; i++)
			buf[PAGE_SIZE] += buf[i];

		Write(sysex_start);
		Write(write_page);
		for (i = 0; i < sizeof(buf); i++) {
			putchar(buf[i] >> 4);
			putchar(buf[i] & 0xf);
		}
		Write(sysex_end);
	}

	Write(sysex_start);
	Write(finalize);
	Write(sysex_end);

	return 0;
}

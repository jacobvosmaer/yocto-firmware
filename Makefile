PRG            = yocto
OBJ            = yocto.o midi.o

MCU_TARGET     = atmega1284p
OPTIMIZE       = -Os -flto
DEFS           = -DF_CPU=16000000L
LIBS           =

# You should not have to change anything below here.

CC             = avr-gcc

# Override is only needed by avr-lib build system.

override CFLAGS        = -g -Wall -pedantic $(OPTIMIZE) -std=gnu89 -mmcu=$(MCU_TARGET) $(DEFS) -Imidiparser
override LDFLAGS       = -Wl,-Map,$(PRG).map

OBJCOPY        = avr-objcopy
OBJDUMP        = avr-objdump

all: $(PRG).elf lst text

midi.o: midiparser/midi.c
	$(CC) $(CFLAGS) -c -o $@ $^

$(PRG).elf: $(OBJ)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^ $(LIBS)

clean:
	rm -f $(OBJ) $(PRG).bin $(PRG).elf $(PRG).lst $(PRG).map
	rm -f bin2syx $(PRG).syx

lst:  $(PRG).lst

%.lst: %.elf
	$(OBJDUMP) -h -S $< > $@

# Rules for building the .text rom images

text: bin syx
	wc -c $(PRG).bin

bin:  $(PRG).bin
syx:  $(PRG).syx

%.bin: %.elf
	$(OBJCOPY) -j .text -j .data -O binary $< $@

%.syx: %.bin bin2syx
	./bin2syx < $< > $@

format:
	clang-format -i -style=file *.c *.h

bin2syx: bin2syx.c
	cc -o bin2syx bin2syx.c

# MIDI-only firmware for the e-licktronic Yocto 1

This is alternative, minimal firmware for the Yocto v1 DIY drum
machine. This firmware only implements an 'expander mode', and
offers no internal sequencer.

Use the mode selector knob to select the MIDI input channel.

Alternatively you can connect a DMUX cable to the "DinSync" port. The
Yocto listens to the first 14 triggers in the DMUX chain, in the order
listed on the hardware. Note that Accent is also a DMUX "note" you
need to trigger. DMUX support requires a jumper on the jacks board to
connect pin 5 of the DinSync socket to PORTB0 of the MCU.

